/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saksit.oxprogramoop;

/**
 *
 * @author User
 */
public class Table {
    private char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private Player winner ;
    private boolean finish = false;
    private int lastCol;
    private int lastRow;
    private int input;
    private char player = 'X';
    private char player2 = 'O';
    private char winner2 = '-';
    
    public Table(Player x,Player o){
        playerX = x;
        playerO = o;
        currentPlayer = x;
    }
    public void showTable(){
        System.out.println(" 1 2 3");
        for(int i=0 ;i < table.length;i++){
            System.out.print((i+1) + " ");
            for(int j=0 ;j < table[i].length;j++){
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }
    public boolean setRowCol(int row,int col){
        if(table[row][col] == '-'){
            table[row][col] = currentPlayer.getName();
            this.lastRow = row;
            this.lastCol = col;
            input++;
           return true;
        }
        return false;
    }
    public Player getCurrentPlayer(){
        return currentPlayer;
    }
    public void switchPlayer(){
        if(currentPlayer == playerX){
            currentPlayer = playerO;
        }else{
            currentPlayer = playerX;
        }
    }
    void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastCol] != currentPlayer.getName()) {
                return;
            }
        }
        showTable();
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    private void setStatWinLose() {
        if(currentPlayer == playerO){
            playerO.win();
            playerX.win();
        }else{
            playerO.lose();
            playerX.win();
        }
    }

     void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastRow][col] != currentPlayer.getName()) {
                return;
            }
        }
        showTable();
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

     void checkx() {
        if (table[0][0] == player && table[1][1] == player && table[2][2] == player) {
            showTable();
            finish = true;
            winner = currentPlayer;
        } else if (table[0][2] == player && table[1][1] == player && table[2][0] == player) {
            showTable();
            finish = true;
            winner = currentPlayer;
        }else if (table[0][0] == player2 && table[1][1] == player2 && table[2][2] == player2) {
            showTable();
            finish = true;
            winner = currentPlayer;
        }else if (table[0][2] == player2 && table[1][1] == player2 && table[2][0] == player2) {
            showTable();
            finish = true;
            winner = currentPlayer;
        }

    }

     void checkDraw() {
        if(input == 8){
           
            showTable();
            finish = true;
            winner2 = '-';
           
        }
         
    }

     public void checkWin() {
        checkRow();
        checkCol();
        checkx();
        checkDraw();
    }
       public boolean isFinish(){
           return finish;
       } 
       public Player getWinner(){
           return winner;
       }
}
