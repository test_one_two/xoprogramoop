/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.saksit.oxprogramoop.Player;
import com.saksit.oxprogramoop.Table;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author User
 */
public class TestTable {
    
    public TestTable() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testRow1Byx(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(x,table.getWinner());
    }
    @Test
    public void testRow2Byx(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(x,table.getWinner());
    }
    @Test
    public void testRow3Byx(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(x,table.getWinner());
    }
    @Test
    public void testCol1Byx(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(x,table.getWinner());
    }
    @Test
     public void testCol2Byx(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(x,table.getWinner());
    }
     @Test
     public void testCol3Byx(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(x,table.getWinner());
    }
     @Test
     public void testcheckxByx(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(x,table.getWinner());
    }
      @Test
     public void testcheckx2Byx(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 2);
        table.setRowCol(1, 1);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(x,table.getWinner());
    }
     @Test
    public void testRow1Byo(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o,x);
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(o,table.getWinner());
    }
    @Test
    public void testRow2Byo(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o,x);
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(o,table.getWinner());
    }
    @Test
    public void testRow3Byo(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o,x);
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(o,table.getWinner());
    }
    @Test
    public void testCol1Byo(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o,x);
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(o,table.getWinner());
    }
    @Test
     public void testCol2Byo(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o,x);
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(o,table.getWinner());
    }
     @Test
     public void testCol3Byo(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o,x);
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(o,table.getWinner());
    }
     @Test
     public void testcheckxByo(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o,x);
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(o,table.getWinner());
    }
     @Test
     public void testcheckx2Byo(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o,x);
        table.setRowCol(0, 2);
        table.setRowCol(1, 1);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(o,table.getWinner());
    }
     @Test
     public void testcheckxdraw1(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(false,table.isFinish());
        assertEquals(null,table.getWinner());
    }
     @Test
     public void testcheckodraw1(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o,x);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.setRowCol(1, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(false,table.isFinish());
        assertEquals(null,table.getWinner());
    }
      @Test
     public void testcheckodraw2(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o,x);
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(1, 2);
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(false,table.isFinish());
        assertEquals(null,table.getWinner());
    }
     @Test
     public void testcheckxdraw2(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 2);
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(false,table.isFinish());
        assertEquals(null,table.getWinner());
    }
     @Test
     public void testcheckodraw3(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o,o);
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(false,table.isFinish());
        assertEquals(null,table.getWinner());
    }
     @Test
     public void testcheckxdraw3(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.setRowCol(1, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(false,table.isFinish());
        assertEquals(null,table.getWinner());
    } @Test
     public void testcheckxdraw4(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(1, 2);
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(false,table.isFinish());
        assertEquals(null,table.getWinner());
    }
     @Test
     public void testcheckodraw4(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o,x);
        table.setRowCol(0, 2);
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(false,table.isFinish());
        assertEquals(null,table.getWinner());
    }
    
}
